import _pt from "prop-types";
import React from "react";
function User(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.id);
}
export const userPropTypes = {
  /* Base(3)*/id: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  /* Common(3)*/username: _pt.string.isRequired,
  /** First name of the current user from their personal details */firstName: _pt.string.isRequired,
  /** Last name of the current user from their personal details */lastName: _pt.string.isRequired,
  slug: _pt.string.isRequired,
  email: _pt.string.isRequired,
  /* This can be either "light", "dark" or "system"*/
  /* The theme setting set by the user in preferences.*/
  preferredTheme: _pt.oneOf(["light", "dark", "system"]).isRequired,
  /** Indicates which locale should be used */preferredLocale: _pt.string.isRequired,
  /* Property(3)*/loginType: _pt.string.isRequired,
  originalUrl: _pt.string,
  thumbnailUrl: _pt.string
};
