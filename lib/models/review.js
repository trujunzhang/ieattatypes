import _pt from "prop-types";
import React from "react";
function Review(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const reviewPropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.string.isRequired,
  /* Common(2)*/rate: _pt.number.isRequired,
  body: _pt.string.isRequired,
  /* user(2)*/username: _pt.string.isRequired,
  avatarUrl: _pt.string.isRequired,
  /* point(4)*/reviewType: _pt.string.isRequired,
  restaurantId: _pt.string,
  eventId: _pt.string,
  recipeId: _pt.string
};
