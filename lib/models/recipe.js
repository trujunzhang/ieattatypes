import _pt from "prop-types";
import React from "react";
function Recipe(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const recipePropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.string.isRequired,
  /* Common(5)*/displayName: _pt.string.isRequired,
  slug: _pt.string.isRequired,
  price: _pt.string.isRequired,
  originalUrl: _pt.string.isRequired,
  thumbnailUrl: _pt.string.isRequired,
  /* for review(2)*/rate: _pt.number.isRequired,
  reviewCount: _pt.number.isRequired,
  /* point(1)*/restaurantId: _pt.string.isRequired
};
