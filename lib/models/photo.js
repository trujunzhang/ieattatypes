import _pt from "prop-types";
import React from "react";
function Photo(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const photoPropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.string.isRequired,
  /* user(2)*/username: _pt.string.isRequired,
  avatarUrl: _pt.string.isRequired,
  /* Common(3)*/originalUrl: _pt.string.isRequired,
  thumbnailUrl: _pt.string.isRequired,
  /* extra*/extraNote: _pt.string.isRequired,
  /* point(4)*/photoType: _pt.string.isRequired,
  restaurantId: _pt.string,
  recipeId: _pt.string,
  userId: _pt.string,
  /* offline(1)*/
  /* status: IFBPhotoStatus*/
  /* photo's status*/
  /* longitude: number*/
  /* latitude: number*/
  /* geoHash: string*/
  /* Location(3)*/
  offlinePath: _pt.string.isRequired
};
