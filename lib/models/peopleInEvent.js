import _pt from "prop-types";
import React from "react";
function PeopleInEvent(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const peopleInEventPropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.string.isRequired,
  /* Common(1)*/recipeIds: _pt.arrayOf(_pt.string).isRequired,
  /* point(3)*/restaurantId: _pt.string.isRequired,
  eventId: _pt.string.isRequired,
  userId: _pt.string.isRequired
};
