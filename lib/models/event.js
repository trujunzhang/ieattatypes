import _pt from "prop-types";
import React from "react";
function Event(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const eventPropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.string.isRequired,
  /* Common(5+1)*/displayName: _pt.string.isRequired,
  slug: _pt.string.isRequired,
  want: _pt.string.isRequired,
  start: _pt.string.isRequired,
  end: _pt.string.isRequired,
  waiters: _pt.arrayOf(_pt.string).isRequired,
  /* for review(2)*/rate: _pt.number.isRequired,
  reviewCount: _pt.number.isRequired,
  /* point(1)*/restaurantId: _pt.string.isRequired
};
