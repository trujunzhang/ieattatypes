import _pt from "prop-types";
import React from "react";
function Restaurant(props) {
  return /*#__PURE__*/React.createElement(React.Fragment, null, props.uniqueId);
}
export const restaurantPropTypes = {
  /* Base(5)*/uniqueId: _pt.string.isRequired,
  flag: _pt.string.isRequired,
  createdAt: _pt.string.isRequired,
  updatedAt: _pt.string.isRequired,
  creatorId: _pt.oneOfType([_pt.string, _pt.oneOf([null])]),
  /* extra(1)*/extraNote: _pt.string.isRequired,
  /* Check google(1)*/isNew: _pt.bool.isRequired,
  /* Location(3)*/geoHash: _pt.string.isRequired,
  latitude: _pt.number.isRequired,
  longitude: _pt.number.isRequired,
  /* Common(4)*/displayName: _pt.string.isRequired,
  slug: _pt.string.isRequired,
  originalUrl: _pt.string.isRequired,
  thumbnailUrl: _pt.string.isRequired,
  /* for review(2)*/rate: _pt.number.isRequired,
  reviewCount: _pt.number.isRequired,
  /* Google api(8)*/address: _pt.string.isRequired,
  street_number: _pt.string.isRequired,
  route: _pt.string.isRequired,
  locality: _pt.string.isRequired,
  sublocality: _pt.string.isRequired,
  country: _pt.string.isRequired,
  postal_code: _pt.string.isRequired,
  administrative_area: _pt.string.isRequired
};
