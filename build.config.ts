import { defineBuildConfig } from "unbuild";

export default defineBuildConfig({
  entries: ["lib/index"],
  // entries: ["src/index"],
  declaration: true,
  clean: true,
  externals: ['react', 'react-dom', "prop-types"],
  rollup: {
    emitCJS: true,
  },
});
