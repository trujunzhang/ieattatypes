# starter-ts

[![Test][gh-ci-test-badge]][gh-ci-test]
[![Lint][gh-ci-lint-badge]][gh-ci-lint]
[![GitHub release][gh-release-badge]][gh-release]

## Tutorial

* For the first release of a private package, similar to `@dup4/starter-ts`, you need to use the following command:
  * `pnpm run release --access public`


## Sponsors

<p align="center">
  <a href="https://github.com/sponsors/Dup4">
    <img src='https://cdn.jsdelivr.net/gh/Dup4/static/sponsors-output/sponsors.svg' alt="Logos from Sponsors" />
  </a>
</p>

## License

[MIT](./LICENSE) License © 2022 - PRESENT [Dup4][dup4]

[dup4]: https://github.com/Dup4
[gh-ci-test-badge]: https://github.com/Dup4/starter-ts/actions/workflows/test.yml/badge.svg
[gh-ci-test]: https://github.com/Dup4/starter-ts/actions/workflows/test.yml
[gh-ci-lint-badge]: https://github.com/Dup4/starter-ts/actions/workflows/lint.yml/badge.svg
[gh-ci-lint]: https://github.com/Dup4/starter-ts/actions/workflows/lint.yml
[gh-release-badge]: https://img.shields.io/github/release/Dup4/starter-ts.svg
[gh-release]: https://GitHub.com/Dup4/starter-ts/releases
