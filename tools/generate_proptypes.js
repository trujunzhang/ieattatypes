/* eslint-disable @typescript-eslint/no-var-requires */
// import { readFile, writeFile } from 'fs-extra'
// import pkg from 'fs-extra'
// const { readFile, writeFile } = pkg
const { readFile, writeFile } = require('fs-extra')
// import {
//     loadConfig,
//     createProgram,
//     parseFromProgram,
//     inject,
// } from 'typescript-to-proptypes'

const {
    loadConfig,
    createProgram,
    parseFromProgram,
    inject,
} = require('typescript-to-proptypes')


// const TS_CONFIG = loadConfig(/* path to tsconfig.json */)
const TS_CONFIG = loadConfig(
    "../tsconfig.json"
)

// type Result = 'success' | 'failed' | 'not-component'

// interface Options {
//   tsDefFilePath: string
//   jsFilePath: string
//   tsProgram: ReturnType<typeof createProgram> // 👈🏾 TS utility type
// }

// const generatePropTypes = async ({
//   tsDefFilePath,
//   jsFilePath,
//   tsProgram,
// }: Options): Promise<Result> => {

const generatePropTypes = async ({
    tsDefFilePath,
    jsFilePath,
    tsProgram,
}) => {
    // `parseFromProgram` retrieves the TS defs for the props,
    // and returns an AST representation of the React Prop Types
    const propTypesAST = parseFromProgram(tsDefFilePath, tsProgram, {
        checkDeclarations: true,
    })

    // if the AST is empty, there are no prop types,
    // which must mean it's not a component
    if (!propTypes.body.length) {
        return 'not-component'
    }

    const jsFileContent = await readFile(jsFilePath, 'utf8')

    // `inject` parses the JS file into an AST & inserts
    // the PropTypes AST based on specified options
    const jsFileContentWithPropTypes = inject(propTypesAST, jsFileContent, {
        removeExistingPropTypes: true,
        babelOptions: {
            filename: jsFilePath,
        },
        comment: `
=============== WARNING ================
| These PropTypes are auto-generated   |
| from the TypeScript type definitions |
========================================
    `,
        reconcilePropTypes: (prop, previous, generated) => {
            if (previous !== undefined) {
                const usedCustomValidator = !previous.startsWith('PropTypes')
                const ignoreGenerated = previous.startsWith(
                    'PropTypes /* @typescript-to-proptypes-ignore */',
                )

                if (usedCustomValidator || ignoreGenerated) {
                    return previous
                }
            }

            return generated
        },

        // include prop type if it's the children prop or if it has a
        // jsDoc description. this helps avoid pulling in prop types
        // for components inheriting base elements like `<a>` or `<input>`
        shouldInclude: ({ prop }) => prop.name === 'children' || !!prop.jsDoc,
    })

    if (!jsFileContentWithPropTypes) {
        return 'failed'
    }

    // OPTIONAL: Use prettier on `jsFileContentWithPropTypes`

    await writeFile(jsFilePath, jsFileContentWithPropTypes)

    return 'success'
}

// `createProgram` comes from the TS Compile API
// create a program passing an array of all the `.d.ts` files for components
// const program = createProgram(tsDefFilePaths, TS_CONFIG)

// The function also assumes that the JS files for the components have
// already been generated/transpiled. Depending on your setup, you would
// use the `babel` or `tsc` CLIs.



// NODE_ENV=production npx babel types/models --extensions '.ts,.tsx,.js,.jsx' --out-dir lib
// NODE_ENV=production npx babel src --extensions '.ts,.tsx,.js,.jsx' --out-dir lib


generatePropTypes({
    tsDefFilePath: 'types/models/index.ts',
    jsFilePath: 'lib/index.js',
    tsProgram: createProgram(['types/models/index.ts'], TS_CONFIG),
})