/* eslint-disable @typescript-eslint/no-var-requires */
// import generate from 'typescript-proptypes-generator';
const generate = require('typescript-proptypes-generator')

generate({
    tsConfig: '../tsconfig.json',
    inputPattern: '../types/models/*.ts',
    outputDir: '../src'
});
